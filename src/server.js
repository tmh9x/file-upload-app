import cors from "cors";
import express from "express";
import fs from "fs";
import multer from "multer";

const app = express();

app.use(
  cors({
    origin: ["http://localhost:5173"],
    methods: ["GET", "POST", "PUT", "DELETE"],
    headers: ["Content-Type", "Authorization"],
  })
);

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./uploads/");
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  },
});

const upload = multer({ storage: storage });

app.post("/upload", upload.single("file"), (req, res) => {
  console.log(req.file);
  res.json({ originalname: req.file.originalname });
});

app.get("/files", (req, res) => {
  const files = fs.readdirSync("./uploads/");
  console.log("files", files);
  res.json(files);
});

app.delete("/files/:filename", (req, res) => {
  const filename = req.params.filename;
  const filePath = `./uploads/${filename}`;

  fs.unlink(filePath, (err) => {
    if (err) {
      console.error(err);
      res.status(500).json({ error: "Failed to delete file" });
    } else {
      res.json({ message: "File deleted successfully" });
    }
  });
});

app.listen(3001, () => {
  console.log("Server listening on port 3001");
});
