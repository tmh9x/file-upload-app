import { useEffect, useState } from "react";

const FileUpload = () => {
  const [file, setFile] = useState(null);
  const [uploadedFiles, setUploadedFiles] = useState([]);

  const handleFileChange = (event) => {
    setFile(event.target.files[0]);
  };

  const handleUpload = () => {
    const formData = new FormData();
    console.log("file.originalname", file.name);
    formData.append("file", file, file.name);

    fetch("http://localhost:3001/upload", {
      method: "POST",
      body: formData,
    })
      .then((response) => response.json())
      .then((data) => {
        console.log("DARTA", data);
        setUploadedFiles((prevFiles) => [...prevFiles, data.originalname]);
      })
      .catch((error) => console.error(error));
  };

  const handleDelete = (filename) => {
    fetch(`http://localhost:3001/files/${filename}`, {
      method: "DELETE",
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error("Failed to delete file");
        }
        return response.json();
      })
      .then(() => {
        setUploadedFiles((prevFiles) =>
          prevFiles.filter((file) => file !== filename)
        );
      })
      .catch((error) => console.error(error));
  };

  const getFiles = () => {
    fetch("http://localhost:3001/files")
      .then((response) => response.json())
      .then((data) => {
        setUploadedFiles(data);
      })
      .catch((error) => console.error(error));
  };

  useEffect(() => {
    getFiles();
  }, []);

  console.log("uploadedFiles", uploadedFiles);
  console.log("file", file);
  return (
    <div className="flex justify-center items-center h-screen bg-gray-900">
      <div className="p-6 bg-gray-800 rounded-lg shadow-lg text-white w-1/2 min-w-fit">
        <label
          htmlFor="file-upload"
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded cursor-pointer"
        >
          Select File
        </label>
        <input
          id="file-upload"
          type="file"
          className="hidden"
          onChange={handleFileChange}
        />
        <button
          className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded cursor-pointer"
          onClick={handleUpload}
        >
          Upload File
        </button>
        <div className="mt-4">
          <h2 className="font-bold text-white mb-2">Uploaded Files:</h2>
          <ul className="text-white">
            {uploadedFiles.map((file, index) => (
              <li
                key={index}
                className="mb-2 p-2 bg-gray-700 rounded flex justify-between items-center"
              >
                <span>{file}</span>
                <button
                  className="bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-2 rounded cursor-pointer"
                  onClick={() => handleDelete(file)}
                >
                  Delete
                </button>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default FileUpload;
