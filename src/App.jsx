import FileUpload from "./components/Fileupload";

function App() {
  return (
    <div>
      <FileUpload />
    </div>
  );
}

export default App;
